package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.IUserRepository;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @NotNull User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    public Boolean isEmailExist(@Nullable final String email) {
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
