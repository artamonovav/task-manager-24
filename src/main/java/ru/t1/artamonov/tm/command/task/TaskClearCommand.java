package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-clear";

    @NotNull
    private static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
